<?php
date_default_timezone_set('Europe/Prague');
session_start();

if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: login.php");
    exit;
}

require_once 'config.php';
?>

<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <title>Majetek TCTV 112</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>
</head>
<body>
<?php
PageHeader();

echo "<table width=\"100%\">";
echo "<tr>";
echo "<th width=\"10\">&nbsp;</th>";
echo "<th >ID</th>";
echo "<th >Lokalita</th>";
echo "<th >&nbsp;</th>";
echo "</tr>";

$i       = 0;
$query60 = "SELECT id, nazev FROM lokality ORDER BY id;";
if ($result60 = mysqli_query($link, $query60)) {
    while ($row60 = mysqli_fetch_row($result60)) {
        $id       = $row60[0];
        $lokalita = $row60[1];

        echo "<tr class=\"";
        if ($i % 2 == 0) {
            echo "dark";
        } else {
            echo "light";
        }
        echo "\"><td>&nbsp;</td><td><a href=\"lokalita.php?id=$id\">$id</a></td><td>$lokalita</td>";
        echo "<td></td></tr>";
        $i = $i + 1;

    }
}
echo "</table>";

mysqli_close($link);
?>