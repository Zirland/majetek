<?php
date_default_timezone_set('Europe/Prague');
session_start();

if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: login.php");
    exit;
}

require_once 'config.php';
$id         = @$_GET["id"];
$id_stanice = @$_POST["id_stanice"];

if (!$id) {
    $id = $id_stanice;
}

$pocitac_new   = @$_POST["pocitac"];
$pc_os_new     = @$_POST["pc_os"];
$monitorL_new  = @$_POST["monitorL"];
$monitorS_new  = @$_POST["monitorS"];
$monitorR_new  = @$_POST["monitorR"];
$soundbar_new  = @$_POST["soundbar"];
$telefon_new   = @$_POST["telefon"];
$tel_set_new   = @$_POST["tel_set"];
$tel_agent_new = @$_POST["tel_agent"];
$tel_ip_new    = @$_POST["tel_ip"];
$extenderL_new = @$_POST["extenderL"];
$extenderS_new = @$_POST["extenderS"];
$extenderR_new = @$_POST["extenderR"];
$kvm_new       = @$_POST["kvm"];
$keyb_new      = @$_POST["klavesnice"];
$mouse_new     = @$_POST["mys"];

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $query36  = "UPDATE stanice SET pocitac='$pocitac_new', monitorL='$monitorL_new', monitorS='$monitorS_new', monitorR='$monitorR_new', soundbar='$soundbar_new', telefon='$telefon_new', extenderL='$extenderL_new', extenderS='$extenderS_new', extenderR='$extenderR_new', kvm='$kvm_new', klavesnice='$keyb_new', mys='$mouse_new' WHERE id='$id_stanice';";
    $prikaz36 = mysqli_query($link, $query36);
    if ($prikaz36 === false) {
        echo "CHYBA: " . mysqli_error($link);
    }

    $query42  = "UPDATE pocitace SET pc_os='$pc_os_new' WHERE service_tag='$pocitac_new';";
    $prikaz42 = mysqli_query($link, $query42);
    if ($prikaz42 === false) {
        echo "CHYBA: " . mysqli_error($link);
    }

    $query48  = "UPDATE telefony SET ip_addr='$tel_ip_new', set_id='$tel_set_new', agent_id='$tel_agent_new' WHERE serial_number='$telefon_new';";
    $prikaz48 = mysqli_query($link, $query48);
    if ($prikaz48 === false) {
        echo "CHYBA: " . mysqli_error($link);
    }

}

?>

<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <title>Pracovní stanice</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <link rel="stylesheet" href="stylesheet.css">
    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>
</head>
<body>
<?php
PageHeader();

$query110 = "SELECT id, placement, ip_addr, pocitac, monitorL, monitorS, monitorR, soundbar, telefon, extenderL, extenderS, extenderR, kvm, klavesnice, mys FROM stanice WHERE id = '$id';";
if ($result110 = mysqli_query($link, $query110)) {
    while ($row110 = mysqli_fetch_row($result110)) {
        $stanice_id = $row110[0];
        $placement  = $row110[1];
        $ip_adresa  = $row110[2];
        $pocitac    = $row110[3];
        $monitorL   = $row110[4];
        $monitorS   = $row110[5];
        $monitorR   = $row110[6];
        $soundbar   = $row110[7];
        $telefon    = $row110[8];
        $extenderL  = $row110[9];
        $extenderS  = $row110[10];
        $extenderR  = $row110[11];
        $kvm        = $row110[12];
        $klavesnice = $row110[13];
        $mys        = $row110[14];
    }
}

echo "<form action=\"" . htmlspecialchars($_SERVER["PHP_SELF"]) . "\" method=\"post\">";
echo "<input type=\"hidden\" name=\"id_stanice\" value=\"$id\">";
echo "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse:collapse;table-layout:fixed\">";
echo "<tr height=\"20\">";
echo "<td colspan=\"13\" height=\"20\" class=\"xl69\"></td>";
echo "</tr>";
echo "<tr height=\"20\">";
echo "<td rowspan=\"3\" height=\"60\" class=\"xl70\"></td>";
echo "<td colspan=\"3\" rowspan=\"3\" class=\"xl65\" style=\"border-right:1px solid black;border-bottom:1px solid black\">";
$query82 = "SELECT mon_typ FROM monitory WHERE serial_number='$monitorL';";
if ($result82 = mysqli_query($link, $query82)) {
    while ($row82 = mysqli_fetch_row($result82)) {
        $mon_typ = $row82[0];

        echo "$mon_typ<br/>";
    }
}
echo "<select name=\"monitorL\"><option value=\"\">---</option>";

$sql = "SELECT serial_number FROM monitory WHERE placement='$placement' AND (serial_number NOT IN (SELECT monitorL FROM stanice) AND serial_number NOT IN (SELECT monitorS FROM stanice) AND serial_number NOT IN (SELECT monitorR FROM stanice) OR serial_number = '$monitorL') ORDER BY serial_number;";
if ($stmt = mysqli_prepare($link, $sql)) {
    if (mysqli_stmt_execute($stmt)) {
        mysqli_stmt_bind_result($stmt, $ser_num);

        while (mysqli_stmt_fetch($stmt)) {
            echo "<option value=\"$ser_num\"";
            if ($ser_num == $monitorL) {
                echo " SELECTED";
            }
            echo ">$ser_num</option>\n";
        }
    }
}
mysqli_stmt_close($stmt);

echo "</select></td>";
echo "<td rowspan=\"3\" class=\"xl78\"></td>";
echo "<td colspan=\"3\" rowspan=\"3\" class=\"xl65\" style=\"border-right:1px solid black;border-bottom:1px solid black\">";
$query82 = "SELECT mon_typ FROM monitory WHERE serial_number='$monitorS';";
if ($result82 = mysqli_query($link, $query82)) {
    while ($row82 = mysqli_fetch_row($result82)) {
        $mon_typ = $row82[0];

        echo "$mon_typ<br/>";
    }
}
echo "<select name=\"monitorS\"><option value=\"\">---</option>";

$sql = "SELECT serial_number FROM monitory WHERE placement='$placement' AND (serial_number NOT IN (SELECT monitorL FROM stanice) AND serial_number NOT IN (SELECT monitorS FROM stanice) AND serial_number NOT IN (SELECT monitorR FROM stanice) OR serial_number = '$monitorS') ORDER BY serial_number;";
if ($stmt = mysqli_prepare($link, $sql)) {
    if (mysqli_stmt_execute($stmt)) {
        mysqli_stmt_bind_result($stmt, $ser_num);

        while (mysqli_stmt_fetch($stmt)) {
            echo "<option value=\"$ser_num\"";
            if ($ser_num == $monitorS) {
                echo " SELECTED";
            }
            echo ">$ser_num</option>\n";
        }
    }
}
mysqli_stmt_close($stmt);

echo "</select></td>";
echo "<td rowspan=\"3\" class=\"xl78\"></td>";
echo "<td colspan=\"3\" rowspan=\"3\" class=\"xl65\" style=\"border-right:1px solid black;border-bottom:1px solid black\">";
$query82 = "SELECT mon_typ FROM monitory WHERE serial_number='$monitorR';";
if ($result82 = mysqli_query($link, $query82)) {
    while ($row82 = mysqli_fetch_row($result82)) {
        $mon_typ = $row82[0];

        echo "$mon_typ<br/>";
    }
}
echo "<select name=\"monitorR\"><option value=\"\">---</option>";

$sql = "SELECT serial_number FROM monitory WHERE placement='$placement' AND (serial_number NOT IN (SELECT monitorL FROM stanice) AND serial_number NOT IN (SELECT monitorS FROM stanice) AND serial_number NOT IN (SELECT monitorR FROM stanice) OR serial_number = '$monitorR') ORDER BY serial_number;";
if ($stmt = mysqli_prepare($link, $sql)) {
    if (mysqli_stmt_execute($stmt)) {
        mysqli_stmt_bind_result($stmt, $ser_num);

        while (mysqli_stmt_fetch($stmt)) {
            echo "<option value=\"$ser_num\"";
            if ($ser_num == $monitorR) {
                echo " SELECTED";
            }
            echo ">$ser_num</option>\n";
        }
    }
}
mysqli_stmt_close($stmt);

echo "</select></td>";
echo "<td rowspan=\"3\" class=\"xl68\"></td>";
echo "</tr>";
echo "<tr height=\"20\">";
echo "</tr>";
echo "<tr height=\"20\">";
echo "</tr>";
echo "<tr height=\"20\">";
echo "<td colspan=\"5\" height=\"20\" class=\"xl69\" style=\"border-right:1px solid black\"></td>";
echo "<td colspan=\"3\" class=\"xl74\" style=\"border-right:1px solid black;border-left:none\">";
$query82 = "SELECT popis FROM soundbary WHERE placement='$placement' AND serial_number='$soundbar';";
if ($result82 = mysqli_query($link, $query82)) {
    while ($row82 = mysqli_fetch_row($result82)) {
        $popis = $row82[0];

        echo "$popis<br/>";
    }
}
echo "<select name=\"soundbar\"><option value=\"\">---</option>";

$sql = "SELECT serial_number FROM soundbary WHERE placement='$placement' AND (serial_number NOT IN (SELECT soundbar FROM stanice) OR serial_number = '$soundbar') ORDER BY serial_number;";
if ($stmt = mysqli_prepare($link, $sql)) {
    if (mysqli_stmt_execute($stmt)) {
        mysqli_stmt_bind_result($stmt, $ser_num);

        while (mysqli_stmt_fetch($stmt)) {
            echo "<option value=\"$ser_num\"";
            if ($ser_num == $soundbar) {
                echo " SELECTED";
            }
            echo ">$ser_num</option>\n";
        }
    }
}
mysqli_stmt_close($stmt);

echo "</select></td>";
echo "<td colspan=\"5\" class=\"xl68\" style=\"border-left:none\"></td>";
echo "</tr>";
echo "<tr height=\"20\">";
echo "<td colspan=\"13\" height=\"20\" class=\"xl69\"></td>";
echo "</tr>";
echo "<tr height=\"20\">";
echo "<td height=\"20\"></td>";
echo "<td colspan=\"3\" class=\"xl74\" style=\"border-right:1px solid black\">";
$query82 = "SELECT popis FROM extendery WHERE serial_number='$extenderL';";
if ($result82 = mysqli_query($link, $query82)) {
    while ($row82 = mysqli_fetch_row($result82)) {
        $popis = $row82[0];

        echo "$popis<br/>";
    }
}
echo "<select name=\"extenderL\"><option value=\"\">---</option>";

$sql = "SELECT serial_number FROM extendery WHERE placement='$placement' AND (serial_number NOT IN (SELECT extenderL FROM stanice) AND serial_number NOT IN (SELECT extenderS FROM stanice) AND serial_number NOT IN (SELECT extenderR FROM stanice) OR serial_number = '$extenderL') ORDER BY serial_number;";
if ($stmt = mysqli_prepare($link, $sql)) {
    if (mysqli_stmt_execute($stmt)) {
        mysqli_stmt_bind_result($stmt, $ser_num);

        while (mysqli_stmt_fetch($stmt)) {
            echo "<option value=\"$ser_num\"";
            if ($ser_num == $extenderL) {
                echo " SELECTED";
            }
            echo ">$ser_num</option>\n";
        }
    }
}
mysqli_stmt_close($stmt);

echo "</select></td>";
echo "<td></td>";
echo "<td colspan=\"3\" class=\"xl74\" style=\"border-right:1px solid black\">";
$query82 = "SELECT popis FROM extendery WHERE serial_number='$extenderS';";
if ($result82 = mysqli_query($link, $query82)) {
    while ($row82 = mysqli_fetch_row($result82)) {
        $popis = $row82[0];

        echo "$popis<br/>";
    }
}
echo "<select name=\"extenderS\"><option value=\"\">---</option>";

$sql = "SELECT serial_number FROM extendery WHERE placement='$placement' AND (serial_number NOT IN (SELECT extenderL FROM stanice) AND serial_number NOT IN (SELECT extenderS FROM stanice) AND serial_number NOT IN (SELECT extenderR FROM stanice) OR serial_number = '$extenderS') ORDER BY serial_number;";
if ($stmt = mysqli_prepare($link, $sql)) {
    if (mysqli_stmt_execute($stmt)) {
        mysqli_stmt_bind_result($stmt, $ser_num);

        while (mysqli_stmt_fetch($stmt)) {
            echo "<option value=\"$ser_num\"";
            if ($ser_num == $extenderS) {
                echo " SELECTED";
            }
            echo ">$ser_num</option>\n";
        }
    }
}
mysqli_stmt_close($stmt);

echo "</select></td>";
echo "<td></td>";
echo "<td colspan=\"3\" class=\"xl74\" style=\"border-right:1px solid black\">";
$query82 = "SELECT popis FROM extendery WHERE serial_number='$extenderR';";
if ($result82 = mysqli_query($link, $query82)) {
    while ($row82 = mysqli_fetch_row($result82)) {
        $popis = $row82[0];

        echo "$popis<br/>";
    }
}
echo "<select name=\"extenderR\"><option value=\"\">---</option>";

$sql = "SELECT serial_number FROM extendery WHERE placement='$placement' AND (serial_number NOT IN (SELECT extenderL FROM stanice) AND serial_number NOT IN (SELECT extenderS FROM stanice) AND serial_number NOT IN (SELECT extenderR FROM stanice) OR serial_number = '$extenderR') ORDER BY serial_number;";
if ($stmt = mysqli_prepare($link, $sql)) {
    if (mysqli_stmt_execute($stmt)) {
        mysqli_stmt_bind_result($stmt, $ser_num);

        while (mysqli_stmt_fetch($stmt)) {
            echo "<option value=\"$ser_num\"";
            if ($ser_num == $extenderR) {
                echo " SELECTED";
            }
            echo ">$ser_num</option>\n";
        }
    }
}
mysqli_stmt_close($stmt);

echo "</select></td>";
echo "<td></td>";
echo "</tr>";
echo "<tr height=\"20\">";
echo "<td colspan=\"13\" height=\"20\" class=\"xl69\"></td>";
echo "</tr>";
echo "<tr height=\"20\">";
echo "<td colspan=\"5\" rowspan=\"2\" height=\"40\" class=\"xl69\" style=\"border-right:1px solid black\">$stanice_id<br/>$ip_adresa</td>";
echo "<td colspan=\"3\" rowspan=\"2\" class=\"xl65\" style=\"border-right:1px solid black;border-bottom:1px solid black\">";
$query82 = "SELECT pc_typ, pc_os, RAM, SATA FROM pocitace WHERE service_tag='$pocitac';";
if ($result82 = mysqli_query($link, $query82)) {
    while ($row82 = mysqli_fetch_row($result82)) {
        $pc_typ = $row82[0];
        $pc_os  = $row82[1];
        $pc_ram = $row82[2];
        $pc_hdd = $row82[3];

        echo "$pc_typ<br/>";
    }
}
echo "<select name=\"pocitac\"><option value=\"\">---</option>";

$sql = "SELECT service_tag FROM pocitace WHERE placement='$placement' AND (service_tag NOT IN (SELECT pocitac FROM stanice) OR service_tag = '$pocitac') ORDER BY service_tag;";
if ($stmt = mysqli_prepare($link, $sql)) {
    if (mysqli_stmt_execute($stmt)) {
        mysqli_stmt_bind_result($stmt, $ser_num);

        while (mysqli_stmt_fetch($stmt)) {
            echo "<option value=\"$ser_num\"";
            if ($ser_num == $pocitac) {
                echo " SELECTED";
            }
            echo ">$ser_num</option>\n";
        }
    }
}
mysqli_stmt_close($stmt);

echo "</select><br/>";
echo "<input type=\"text\" name=\"pc_os\" value=\"$pc_os\"><br/>";
echo "$pc_ram GB RAM, $pc_hdd GB HDD";
echo "</td>";

echo "<td rowspan=\"2\" class=\"xl78\"></td>";
echo "<td colspan=\"2\" rowspan=\"2\" class=\"xl65\" style=\"border-right:1px solid black;border-bottom:1px solid black\">";
$query82 = "SELECT popis FROM kvm WHERE serial_number='$kvm';";
if ($result82 = mysqli_query($link, $query82)) {
    while ($row82 = mysqli_fetch_row($result82)) {
        $popis = $row82[0];

        echo "$popis<br/>";
    }
}
echo "<select name=\"kvm\"><option value=\"\">---</option>";

$sql = "SELECT serial_number FROM kvm WHERE placement='$placement' AND (serial_number NOT IN (SELECT kvm FROM stanice) OR serial_number = '$kvm') ORDER BY serial_number;";
if ($stmt = mysqli_prepare($link, $sql)) {
    if (mysqli_stmt_execute($stmt)) {
        mysqli_stmt_bind_result($stmt, $ser_num);

        while (mysqli_stmt_fetch($stmt)) {
            echo "<option value=\"$ser_num\"";
            if ($ser_num == $kvm) {
                echo " SELECTED";
            }
            echo ">$ser_num</option>\n";
        }
    }
}
mysqli_stmt_close($stmt);

echo "</select></td>";
echo "<td colspan=\"2\" rowspan=\"2\" class=\"xl68\"></td>";
echo "</tr>";
echo "<tr height=\"20\">";
echo "</tr>";
echo "<tr height=\"20\">";
echo "<td colspan=\"13\" height=\"20\" class=\"xl69\"></td>";
echo "</tr>";
echo "<tr height=\"20\">";
echo "<td rowspan=\"2\" height=\"40\" class=\"xl69\"></td>";
echo "<td rowspan=\"2\" class=\"xl65\" style=\"border-bottom:1px solid black\"></td>";
echo "<td colspan=\"2\" class=xl75></td>";
echo "<td rowspan=\"2\" class=xl67 style=\"border-bottom:1px solid black\"></td>";
echo "<td colspan=\"8\" rowspan=\"2\" class=\"xl69\"></td>";
echo "</tr>";
echo "<tr height=\"20\">";
echo "<td colspan=\"2\" rowspan=\"2\" height=\"40\" class=\"xl65\" style=\"border-right:1px solid black;border-bottom:1px solid black\">";
$query82 = "SELECT tel_typ, set_id, agent_id, ip_addr FROM telefony WHERE serial_number='$telefon';";
if ($result82 = mysqli_query($link, $query82)) {
    while ($row82 = mysqli_fetch_row($result82)) {
        $tel_typ   = $row82[0];
        $tel_set   = $row82[1];
        $tel_agent = $row82[2];
        $tel_ip    = $row82[3];

        echo "$tel_typ<br/>";
    }
}
echo "<select name=\"telefon\"><option value=\"\">---</option>";

$sql = "SELECT serial_number FROM telefony WHERE placement='$placement' AND (serial_number NOT IN (SELECT telefon FROM stanice) OR serial_number = '$telefon') ORDER BY serial_number;";
if ($stmt = mysqli_prepare($link, $sql)) {
    if (mysqli_stmt_execute($stmt)) {
        mysqli_stmt_bind_result($stmt, $ser_num);

        while (mysqli_stmt_fetch($stmt)) {
            echo "<option value=\"$ser_num\"";
            if ($ser_num == $telefon) {
                echo " SELECTED";
            }
            echo ">$ser_num</option>\n";
        }
    }
}
mysqli_stmt_close($stmt);

echo "</select><br/>";
echo "Set <input type=\"text\" name=\"tel_set\" value=\"$tel_set\" size=\"4\"> Agent <input type=\"text\" name=\"tel_agent\" value=\"$tel_agent\" size=\"4\"><br/>";
echo "<input type=\"text\" name=\"tel_ip\" value=\"$tel_ip\"><br/>";
echo "</td>";
echo "</tr>";
echo "<tr height=\"20\">";
echo "<td colspan=\"2\" height=\"20\" class=\"xl69\"></td>";
echo "<td colspan=\"2\" class=\"xl69\" style=\"border-right:1px solid black\"></td>";
echo "<td colspan=\"2\" class=\"xl74\" style=\"border-right:1px solid black;border-left:none\">";
$query82 = "SELECT popis FROM klavesnice WHERE serial_number='$klavesnice';";
if ($result82 = mysqli_query($link, $query82)) {
    while ($row82 = mysqli_fetch_row($result82)) {
        $popis = $row82[0];

        echo "$popis<br/>";
    }
}
echo "<select name=\"klavesnice\"><option value=\"\">---</option>";

$sql = "SELECT serial_number FROM klavesnice WHERE placement='$placement' AND (serial_number NOT IN (SELECT klavesnice FROM stanice) OR serial_number = '$klavesnice') ORDER BY serial_number;";
if ($stmt = mysqli_prepare($link, $sql)) {
    if (mysqli_stmt_execute($stmt)) {
        mysqli_stmt_bind_result($stmt, $ser_num);

        while (mysqli_stmt_fetch($stmt)) {
            echo "<option value=\"$ser_num\"";
            if ($ser_num == $klavesnice) {
                echo " SELECTED";
            }
            echo ">$ser_num</option>\n";
        }
    }
}
mysqli_stmt_close($stmt);

echo "</select></td>";

echo "<td></td>";
echo "<td colspan=\"2\" class=xl77>";
$query82 = "SELECT popis FROM mysi WHERE serial_number='$mys';";
if ($result82 = mysqli_query($link, $query82)) {
    while ($row82 = mysqli_fetch_row($result82)) {
        $popis = $row82[0];

        echo "$popis<br/>";
    }
}
echo "<select name=\"mys\"><option value=\"\">---</option>";

$sql = "SELECT serial_number FROM mysi WHERE placement='$placement' AND (serial_number NOT IN (SELECT mys FROM stanice) OR serial_number = '$mys') ORDER BY serial_number;";
if ($stmt = mysqli_prepare($link, $sql)) {
    if (mysqli_stmt_execute($stmt)) {
        mysqli_stmt_bind_result($stmt, $ser_num);

        while (mysqli_stmt_fetch($stmt)) {
            echo "<option value=\"$ser_num\"";
            if ($ser_num == $mys) {
                echo " SELECTED";
            }
            echo ">$ser_num</option>\n";
        }
    }
}
mysqli_stmt_close($stmt);

echo "</select></td>";

echo "<td colspan=\"2\" class=\"xl68\" style=\"border-left:none\"></td>";
echo "</tr>";
echo "<tr height=\"20\">";
echo "<td colspan=\"13\" height=\"20\" class=\"xl69\"></td>";
echo "</tr>";
echo "</table>";

echo "<input type=\"submit\" value=\"Uložit\"></form>";

mysqli_close($link);
?>