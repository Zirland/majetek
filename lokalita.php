<?php
date_default_timezone_set('Europe/Prague');
session_start();

if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: login.php");
    exit;
}

require_once 'config.php';

$lokalita    = @$_GET["id"];
$id_lokalita = @$_POST["id_lokalita"];

if (!$lokalita) {
    $lokalita = $id_lokalita;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    foreach ($_POST as $key => $value) {
        $command = "";
        if (preg_match('/T-.+/', $key, $serial)) {
            $command = "UPDATE tiskarny SET ";
            if (preg_match('/.+poznamka/', $key, $pozn)) {
                $serial_value = explode("-", $pozn[0])[1];
                $command .= "poznamka = '$value' WHERE serial_number ='$serial_value';";
            } else {
                $serial_value = substr($serial[0], 2);
                $command .= "placement = '$value' WHERE serial_number ='$serial_value';";
            }
        }
        if (preg_match('/C-.+/', $key, $serial)) {
            $command = "UPDATE pocitace SET ";
            if (preg_match('/.+poznamka/', $key, $pozn)) {
                $serial_value = explode("-", $pozn[0])[1];
                $command .= "poznamka = '$value' WHERE service_tag ='$serial_value';";
            } else {
                $serial_value = substr($serial[0], 2);
                $command .= "placement = '$value' WHERE service_tag ='$serial_value';";
            }
        }
        if (preg_match('/M-.+/', $key, $serial)) {
            $command = "UPDATE monitory SET ";
            if (preg_match('/.+poznamka/', $key, $pozn)) {
                $serial_value = explode("-", $pozn[0])[1];
                $command .= "poznamka = '$value' WHERE serial_number ='$serial_value';";
            } else {
                $serial_value = substr($serial[0], 2);
                $command .= "placement = '$value' WHERE serial_number ='$serial_value';";
            }
        }
        if (preg_match('/S-.+/', $key, $serial)) {
            $command = "UPDATE soundbary SET ";
            if (preg_match('/.+poznamka/', $key, $pozn)) {
                $serial_value = explode("-", $pozn[0])[1];
                $command .= "poznamka = '$value' WHERE serial_number ='$serial_value';";
            } else {
                $serial_value = substr($serial[0], 2);
                $command .= "placement = '$value' WHERE serial_number ='$serial_value';";
            }
        }
        if (preg_match('/E-.+/', $key, $serial)) {
            $command = "UPDATE extendery SET ";
            if (preg_match('/.+poznamka/', $key, $pozn)) {
                $serial_value = explode("-", $pozn[0])[1];
                $command .= "poznamka = '$value' WHERE serial_number ='$serial_value';";
            } else {
                $serial_value = substr($serial[0], 2);
                $command .= "placement = '$value' WHERE serial_number ='$serial_value';";
            }
        }
        if (preg_match('/KV-.+/', $key, $serial)) {
            $command = "UPDATE kvm SET ";
            if (preg_match('/.+poznamka/', $key, $pozn)) {
                $serial_value = explode("-", $pozn[0])[1];
                $command .= "poznamka = '$value' WHERE serial_number ='$serial_value';";
            } else {
                $serial_value = substr($serial[0], 3);
                $command .= "placement = '$value' WHERE serial_number ='$serial_value';";
            }
        }
        if (preg_match('/TL-.+/', $key, $serial)) {
            $command = "UPDATE telefony SET ";
            if (preg_match('/.+poznamka/', $key, $pozn)) {
                $serial_value = explode("-", $pozn[0])[1];
                $command .= "poznamka = '$value' WHERE serial_number ='$serial_value';";
            } else {
                $serial_value = substr($serial[0], 3);
                $command .= "placement = '$value' WHERE serial_number ='$serial_value';";
            }
        }
        if (preg_match('/KB-.+/', $key, $serial)) {
            $command = "UPDATE klavesnice SET ";
            if (preg_match('/.+poznamka/', $key, $pozn)) {
                $serial_value = explode("-", $pozn[0])[1];
                $command .= "poznamka = '$value' WHERE serial_number ='$serial_value';";
            } else {
                $serial_value = substr($serial[0], 3);
                $command .= "placement = '$value' WHERE serial_number ='$serial_value';";
            }
        }
        if (preg_match('/MS-.+/', $key, $serial)) {
            $command = "UPDATE mysi SET ";
            if (preg_match('/.+poznamka/', $key, $pozn)) {
                $serial_value = explode("-", $pozn[0])[1];
                $command .= "poznamka = '$value' WHERE serial_number ='$serial_value';";
            } else {
                $serial_value = substr($serial[0], 3);
                $command .= "placement = '$value' WHERE serial_number ='$serial_value';";
            }
        }
        if (preg_match('/EQ-.+/', $key, $serial)) {
            $command = "UPDATE hardware SET ";
            if (preg_match('/.+poznamka/', $key, $pozn)) {
                $serial_value = explode("-", $pozn[0])[1];
                $command .= "poznamka = '$value' WHERE serial_number ='$serial_value';";
            } else {
                $serial_value = substr($serial[0], 3);
                $command .= "placement = '$value' WHERE serial_number ='$serial_value';";
            }
        }

//        echo "71: $command<br/>";
        if ($command != "") {
            $prikaz59 = mysqli_query($link, $command);
        }
    }
}
?>

<!DOCTYPE html>
<html lang="cs">

<head>
    <meta charset="UTF-8">
    <title>Majetek TCTV 112</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body {
            font: 14px sans-serif;
        }

        .wrapper {
            width: 350px;
            padding: 20px;
        }
    </style>
</head>

<body>
    <?php
PageHeader();

$query30 = "SELECT id, nazev FROM lokality where id='$lokalita';";
if ($result30 = mysqli_query($link, $query30)) {
    while ($row30 = mysqli_fetch_row($result30)) {
        $id     = $row30[0];
        $adresa = $row30[1];

        echo "<H1>$adresa</H1>";
    }
}

if ($lokalita != "OUT") {
    echo "<table width=\"100%\">";
    echo "<tr><th>Stanice</th><th>IP adresa</th><th>Počítač</th><th>Monitor L</th><th>Monitor S</th><th>Monitor P</th><th>Soundbar</th><th>Extend L</th><th>Extend S</th><th>Extend P</th><th>KVM</th><th>Telefon</th><th>Klávesnice</th><th>Myš</th></tr>";

    $query41 = "SELECT id, ip_addr, pocitac, monitorL, monitorS, monitorR, soundbar, extenderL, extenderS, extenderR, kvm, telefon, klavesnice, mys FROM stanice WHERE placement='$lokalita';";
    if ($result41 = mysqli_query($link, $query41)) {
        while ($row41 = mysqli_fetch_row($result41)) {
            $stanice    = $row41[0];
            $ip_adresa  = $row41[1];
            $pocitac    = $row41[2];
            $monitorL   = $row41[3];
            $monitorS   = $row41[4];
            $monitorR   = $row41[5];
            $soundbar   = $row41[6];
            $extenderL  = $row41[7];
            $extenderS  = $row41[8];
            $extenderR  = $row41[9];
            $kvm        = $row41[10];
            $telefon    = $row41[11];
            $klavesnice = $row41[12];
            $mys        = $row41[13];

            echo "<tr><td><a href=\"stanice_edit.php?id=$stanice\" target=\"_blank\">$stanice</a></td><td>$ip_adresa</td><td>$pocitac</td><td>$monitorL</td><td>$monitorS</td><td>$monitorR</td><td>$soundbar</td><td>$extenderL</td><td>$extenderS</td><td>$extenderR</td><td>$kvm</td><td>$telefon</td><td>$klavesnice</td><td>$mys</td></tr>";
        }
    }
    echo "</table>";
    echo "<hr/>";
    echo "<p></p>";
}
echo "<table width=\"1000\">";
echo "<form action=\"" . htmlspecialchars($_SERVER["PHP_SELF"]) . "\" method=\"post\">";
echo "<input type=\"hidden\" name=\"id_lokalita\" value=\"$lokalita\"";
$query66 = "SELECT popis, serial_number, poznamka FROM tiskarny WHERE placement='$lokalita' ORDER BY serial_number;";
if ($result66 = mysqli_query($link, $query66)) {
    while ($row66 = mysqli_fetch_row($result66)) {
        $tisk_typ      = $row66[0];
        $serial_number = $row66[1];
        $poznamka      = $row66[2];

        echo "<tr><td>$tisk_typ</td><td>$serial_number</td><td></td><td>";
        echo "<input type=\"text\" name=\"T-$serial_number-poznamka\" value=\"$poznamka\">";
        echo "</td><td>";
        echo "<select name=\"T-$serial_number\">";

        $sql = "SELECT id,nazev FROM lokality ORDER BY nazev;";
        if ($stmt = mysqli_prepare($link, $sql)) {
            if (mysqli_stmt_execute($stmt)) {
                mysqli_stmt_bind_result($stmt, $loc_id, $loc_name);

                while (mysqli_stmt_fetch($stmt)) {
                    echo "<option value=\"$loc_id\"";
                    if ($loc_id == $lokalita) {
                        echo " SELECTED";
                    }
                    echo ">$loc_name</option>\n";
                }
            }
        }
        mysqli_stmt_close($stmt);
        echo "</select>";
        echo "</td></tr>";
    }
}

$query78 = "SELECT pc_typ, service_tag, poznamka FROM pocitace WHERE placement='$lokalita' AND service_tag NOT IN (SELECT pocitac FROM stanice) ORDER BY service_tag;";
if ($result78 = mysqli_query($link, $query78)) {
    while ($row78 = mysqli_fetch_row($result78)) {
        $pc_typ      = $row78[0];
        $service_tag = $row78[1];
        $poznamka    = $row78[2];

        echo "<tr><td>$pc_typ</td><td></td><td>$service_tag</td><td>";
        echo "<input type=\"text\" name=\"C-$service_tag-poznamka\" value=\"$poznamka\">";
        echo "</td><td>";
        echo "<select name=\"C-$service_tag\">";

        $sql = "SELECT id,nazev FROM lokality ORDER BY nazev;";
        if ($stmt = mysqli_prepare($link, $sql)) {
            if (mysqli_stmt_execute($stmt)) {
                mysqli_stmt_bind_result($stmt, $loc_id, $loc_name);

                while (mysqli_stmt_fetch($stmt)) {
                    echo "<option value=\"$loc_id\"";
                    if ($loc_id == $lokalita) {
                        echo " SELECTED";
                    }
                    echo ">$loc_name</option>\n";
                }
            }
        }
        mysqli_stmt_close($stmt);
        echo "</select>";
        echo "</td></tr>";
    }
}

$query90 = "SELECT mon_typ, serial_number, service_tag, poznamka FROM monitory WHERE placement='$lokalita' AND (serial_number NOT IN (SELECT monitorL FROM stanice) AND serial_number NOT IN (SELECT monitorS FROM stanice) AND serial_number NOT IN (SELECT monitorR FROM stanice)) ORDER BY serial_number;";
if ($result90 = mysqli_query($link, $query90)) {
    while ($row90 = mysqli_fetch_row($result90)) {
        $mon_typ       = $row90[0];
        $serial_number = $row90[1];
        $service_tag   = $row90[2];
        $poznamka      = $row90[3];

        echo "<tr><td>$mon_typ</td><td>$serial_number</td><td>$service_tag</td><td>";
        echo "<input type=\"text\" name=\"M-$serial_number-poznamka\" value=\"$poznamka\">";
        echo "</td><td>";
        echo "<select name=\"M-$serial_number\">";

        $sql = "SELECT id,nazev FROM lokality ORDER BY nazev;";
        if ($stmt = mysqli_prepare($link, $sql)) {
            if (mysqli_stmt_execute($stmt)) {
                mysqli_stmt_bind_result($stmt, $loc_id, $loc_name);

                while (mysqli_stmt_fetch($stmt)) {
                    echo "<option value=\"$loc_id\"";
                    if ($loc_id == $lokalita) {
                        echo " SELECTED";
                    }
                    echo ">$loc_name</option>\n";
                }
            }
        }
        mysqli_stmt_close($stmt);
        echo "</select>";
        echo "</td></tr>";
    }
}

$query104 = "SELECT popis, serial_number, poznamka FROM soundbary AND serial_number NOT IN (SELECT soundbar FROM stanice) WHERE placement='$lokalita' ORDER BY serial_number;";
if ($result104 = mysqli_query($link, $query104)) {
    while ($row104 = mysqli_fetch_row($result104)) {
        $sound_typ     = $row104[0];
        $serial_number = $row104[1];
        $poznamka      = $row104[2];

        echo "<tr><td>$sound_typ</td><td>$serial_number</td><td></td><td>";
        echo "<input type=\"text\" name=\"S-$serial_number-poznamka\" value=\"$poznamka\">";
        echo "</td><td>";
        echo "<select name=\"S-$serial_number\">";

        $sql = "SELECT id,nazev FROM lokality ORDER BY nazev;";
        if ($stmt = mysqli_prepare($link, $sql)) {
            if (mysqli_stmt_execute($stmt)) {
                mysqli_stmt_bind_result($stmt, $loc_id, $loc_name);

                while (mysqli_stmt_fetch($stmt)) {
                    echo "<option value=\"$loc_id\"";
                    if ($loc_id == $lokalita) {
                        echo " SELECTED";
                    }
                    echo ">$loc_name</option>\n";
                }
            }
        }
        mysqli_stmt_close($stmt);
        echo "</select>";
        echo "</td></tr>";
    }
}

$query116 = "SELECT popis, serial_number, poznamka FROM extendery WHERE placement='$lokalita' AND (serial_number NOT IN (SELECT extenderL FROM stanice) AND serial_number NOT IN (SELECT extenderS FROM stanice) AND serial_number NOT IN (SELECT extenderR FROM stanice)) ORDER BY serial_number;";
if ($result116 = mysqli_query($link, $query116)) {
    while ($row116 = mysqli_fetch_row($result116)) {
        $extend_typ    = $row116[0];
        $serial_number = $row116[1];
        $poznamka      = $row116[2];

        echo "<tr><td>$extend_typ</td><td>$serial_number</td><td></td><td>";
        echo "<input type=\"text\" name=\"E-$serial_number-poznamka\" value=\"$poznamka\">";
        echo "</td><td>";
        echo "<select name=\"E-$serial_number\">";

        $sql = "SELECT id,nazev FROM lokality ORDER BY nazev;";
        if ($stmt = mysqli_prepare($link, $sql)) {
            if (mysqli_stmt_execute($stmt)) {
                mysqli_stmt_bind_result($stmt, $loc_id, $loc_name);

                while (mysqli_stmt_fetch($stmt)) {
                    echo "<option value=\"$loc_id\"";
                    if ($loc_id == $lokalita) {
                        echo " SELECTED";
                    }
                    echo ">$loc_name</option>\n";
                }
            }
        }
        mysqli_stmt_close($stmt);
        echo "</select>";
        echo "</td></tr>";
    }
}

$query128 = "SELECT popis, serial_number, poznamka FROM kvm AND serial_number NOT IN (SELECT kvm FROM stanice) WHERE placement='$lokalita' ORDER BY serial_number;";
if ($result128 = mysqli_query($link, $query128)) {
    while ($row128 = mysqli_fetch_row($result128)) {
        $kvm_typ       = $row128[0];
        $serial_number = $row128[1];
        $poznamka      = $row128[2];

        echo "<tr><td>$kvm_typ</td><td>$serial_number</td><td></td><td>";
        echo "<input type=\"text\" name=\"KV-$serial_number-poznamka\" value=\"$poznamka\">";
        echo "</td><td>";
        echo "<select name=\"KV-$serial_number\">";

        $sql = "SELECT id,nazev FROM lokality ORDER BY nazev;";
        if ($stmt = mysqli_prepare($link, $sql)) {
            if (mysqli_stmt_execute($stmt)) {
                mysqli_stmt_bind_result($stmt, $loc_id, $loc_name);

                while (mysqli_stmt_fetch($stmt)) {
                    echo "<option value=\"$loc_id\"";
                    if ($loc_id == $lokalita) {
                        echo " SELECTED";
                    }
                    echo ">$loc_name</option>\n";
                }
            }
        }
        mysqli_stmt_close($stmt);
        echo "</select>";
        echo "</td></tr>";
    }
}

$query140 = "SELECT tel_typ, serial_number, poznamka FROM telefony WHERE placement='$lokalita' AND serial_number NOT IN (SELECT telefon FROM stanice) ORDER BY serial_number;";
if ($result140 = mysqli_query($link, $query140)) {
    while ($row140 = mysqli_fetch_row($result140)) {
        $tel_typ       = $row140[0];
        $serial_number = $row140[1];
        $poznamka      = $row140[2];

        echo "<tr><td>$tel_typ</td><td>$serial_number</td><td></td><td>";
        echo "<input type=\"text\" name=\"TL-$serial_number-poznamka\" value=\"$poznamka\">";
        echo "</td><td>";
        echo "<select name=\"TL-$serial_number\">";

        $sql = "SELECT id,nazev FROM lokality ORDER BY nazev;";
        if ($stmt = mysqli_prepare($link, $sql)) {
            if (mysqli_stmt_execute($stmt)) {
                mysqli_stmt_bind_result($stmt, $loc_id, $loc_name);

                while (mysqli_stmt_fetch($stmt)) {
                    echo "<option value=\"$loc_id\"";
                    if ($loc_id == $lokalita) {
                        echo " SELECTED";
                    }
                    echo ">$loc_name</option>\n";
                }
            }
        }
        mysqli_stmt_close($stmt);
        echo "</select>";
        echo "</td></tr>";
    }
}

$query152 = "SELECT popis, serial_number, poznamka FROM klavesnice WHERE placement='$lokalita' AND serial_number NOT IN (SELECT klavesnice FROM stanice) ORDER BY serial_number;";
if ($result152 = mysqli_query($link, $query152)) {
    while ($row152 = mysqli_fetch_row($result152)) {
        $keyb_typ      = $row152[0];
        $serial_number = $row152[1];
        $poznamka      = $row152[2];

        echo "<tr><td>$keyb_typ</td><td>$serial_number</td><td></td><td>";
        echo "<input type=\"text\" name=\"KB-$serial_number-poznamka\" value=\"$poznamka\">";
        echo "</td><td>";
        echo "<select name=\"KB-$serial_number\">";

        $sql = "SELECT id,nazev FROM lokality ORDER BY nazev;";
        if ($stmt = mysqli_prepare($link, $sql)) {
            if (mysqli_stmt_execute($stmt)) {
                mysqli_stmt_bind_result($stmt, $loc_id, $loc_name);

                while (mysqli_stmt_fetch($stmt)) {
                    echo "<option value=\"$loc_id\"";
                    if ($loc_id == $lokalita) {
                        echo " SELECTED";
                    }
                    echo ">$loc_name</option>\n";
                }
            }
        }
        mysqli_stmt_close($stmt);
        echo "</select>";
        echo "</td></tr>";
    }
}

$query164 = "SELECT popis, serial_number, poznamka FROM mysi WHERE placement='$lokalita' AND serial_number NOT IN (SELECT mys FROM stanice) ORDER BY serial_number;";
if ($result164 = mysqli_query($link, $query164)) {
    while ($row164 = mysqli_fetch_row($result164)) {
        $mys_typ       = $row164[0];
        $serial_number = $row164[1];
        $poznamka      = $row164[2];

        echo "<tr><td>$mys_typ</td><td>$serial_number</td><td></td><td>";
        echo "<input type=\"text\" name=\"MS-$serial_number-poznamka\" value=\"$poznamka\">";
        echo "</td><td>";
        echo "<select name=\"MS-$serial_number\">";

        $sql = "SELECT id,nazev FROM lokality ORDER BY nazev;";
        if ($stmt = mysqli_prepare($link, $sql)) {
            if (mysqli_stmt_execute($stmt)) {
                mysqli_stmt_bind_result($stmt, $loc_id, $loc_name);

                while (mysqli_stmt_fetch($stmt)) {
                    echo "<option value=\"$loc_id\"";
                    if ($loc_id == $lokalita) {
                        echo " SELECTED";
                    }
                    echo ">$loc_name</option>\n";
                }
            }
        }
        mysqli_stmt_close($stmt);
        echo "</select>";
        echo "</td></tr>";
    }
}

$query392 = "SELECT popis, serial_number, poznamka FROM hardware WHERE placement='$lokalita' ORDER BY serial_number;";
if ($result392 = mysqli_query($link, $query392)) {
    while ($row392 = mysqli_fetch_row($result392)) {
        $hw_typ        = $row392[0];
        $serial_number = $row392[1];
        $poznamka      = $row392[2];

        echo "<tr><td>$hw_typ</td><td>$serial_number</td><td></td><td>";
        echo "<input type=\"text\" name=\"EQ-$serial_number-poznamka\" value=\"$poznamka\">";
        echo "</td><td>";
        echo "<select name=\"EQ-$serial_number\">";

        $sql = "SELECT id,nazev FROM lokality ORDER BY nazev;";
        if ($stmt = mysqli_prepare($link, $sql)) {
            if (mysqli_stmt_execute($stmt)) {
                mysqli_stmt_bind_result($stmt, $loc_id, $loc_name);

                while (mysqli_stmt_fetch($stmt)) {
                    echo "<option value=\"$loc_id\"";
                    if ($loc_id == $lokalita) {
                        echo " SELECTED";
                    }
                    echo ">$loc_name</option>\n";
                }
            }
        }
        mysqli_stmt_close($stmt);
        echo "</select>";
        echo "</td></tr>";
    }
}

echo "</table>";
echo "<input type=\"submit\" class=\"btn btn-primary\" value=\"Přesunout\">";
echo "</form>";

mysqli_close($link);
?>