<?php
date_default_timezone_set('Europe/Prague');
session_start();

if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: login.php");
    exit;
}

require_once 'config.php';

$search     = @$_POST["search"];
$search_err = "";
$outText    = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty(trim($search))) {
        $search_err = "Zadejte prosím kód majetku.";
    }

    if (empty($search_err)) {
        $outText .= "<table width=\"1000\">";
        $query22 = "SELECT serial_number,popis,placement FROM extendery WHERE (serial_number='$search' OR barcode LIKE '%$search%')
        UNION SELECT serial_number,popis,placement FROM klavesnice WHERE (serial_number='$search' OR barcode LIKE '%$search%')
        UNION SELECT serial_number,popis,placement FROM kvm WHERE (serial_number='$search' OR barcode LIKE '%$search%')
        UNION SELECT serial_number,mon_typ,placement FROM monitory WHERE (serial_number='$search' OR service_tag='$search' OR barcode LIKE '%$search%')
        UNION SELECT serial_number,popis,placement FROM mysi WHERE (serial_number='$search' OR barcode LIKE '%$search%')
        UNION SELECT service_tag,pc_typ,placement FROM pocitace WHERE (service_tag='$search' OR barcode LIKE '%$search%')
        UNION SELECT serial_number,popis,placement FROM soundbary WHERE (serial_number='$search' OR barcode LIKE '%$search%')
        UNION SELECT serial_number,tel_typ,placement FROM telefony WHERE (serial_number='$search' OR barcode LIKE '%$search%')
        UNION SELECT serial_number,tel_typ,placement FROM hardware WHERE (serial_number='$search' OR barcode LIKE '%$search%')
        UNION SELECT serial_number,popis,placement FROM tiskarny WHERE (serial_number='$search' OR barcode LIKE '%$search%');";
        if ($result22 = mysqli_query($link, $query22)) {
            while ($row22 = mysqli_fetch_row($result22)) {
                $serial_number = $row22[0];
                $popis         = $row22[1];
                $placement     = $row22[2];

                $outText .= "<tr><td>$serial_number</td><td>$popis</td><td><a href=\"lokalita.php?id=$placement\">$placement</a></td></tr>";
            }
        }
        $outText .= "</table>";
    }
}
?>

<!DOCTYPE html>
<html lang="cs">

<head>
    <meta charset="UTF-8">
    <title>Vyhledání majetku</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body {
            font: 14px sans-serif;
        }

        .wrapper {
            width: 500px;
            padding: 20px;
        }

        tr.dark {
            background-color: #ddd;
            color: black;
        }

        tr.light {
            background-color: #fff;
            color: black;
        }
    </style>
</head>

<body>
    <?php
    $app_up = PageHeader();
    ?>
    <div class="wrapper">
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group <?php echo (!empty($search_err)) ? 'has-error' : ''; ?>">
                <label>Zadejte kód:</label>
                <input type="text" name="search" class="form-control" value="" autofocus>
                <input type="submit" class="btn btn-primary" value="Vložit">
                <span class="help-block"><?php echo $search_err; ?></span>
            </div>
        </form>
    </div>

    <hr>
    <?php

    echo $outText;

    mysqli_close($link);
    ?>