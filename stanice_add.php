<?php
date_default_timezone_set('Europe/Prague');
session_start();

if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: login.php");
    exit;
}

require_once 'config.php';

$stanice      = @$_POST["stanice"];
$stanice_err  = "";
$lokalita     = @$_POST["lokalita"];
$lokalita_err = "";
$address      = @$_POST["address"];
$address_err  = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty(trim($stanice))) {
        $stanice_err = "Zadejte prosím název pracovní stanice.";
    } else {
        $sql = "SELECT id FROM stanice WHERE id = ?";

        if ($stmt = mysqli_prepare($link, $sql)) {
            mysqli_stmt_bind_param($stmt, "s", $param_stanice);

            $param_stanice = trim($stanice);

            if (mysqli_stmt_execute($stmt)) {
                mysqli_stmt_store_result($stmt);

                if (mysqli_stmt_num_rows($stmt) == 1) {
                    $stanice_err = "Název je již použit.";
                } else {
                    $stanice = trim($stanice);
                }
            } else {
                echo "Něco se nepovedlo. Zkuste to prosím znovu.";
            }
        }
        mysqli_stmt_close($stmt);
    }

    if (empty(trim($lokalita))) {
        $lokalita_err = "Vyberte prosím lokalitu.";
    }

    if (empty($stanice_err) && empty($lokalita_err)) {
        $sql = "INSERT INTO stanice (id, placement, ip_addr) VALUES (?, ?, ?)";

        if ($stmt = mysqli_prepare($link, $sql)) {
            mysqli_stmt_bind_param($stmt, "sss", $param_stanice, $param_lokalita, $param_adresa);

            $param_stanice  = $stanice;
            $param_lokalita = $lokalita;
            $param_adresa   = $address;

            if (mysqli_stmt_execute($stmt)) {
                echo "";
            }
            mysqli_stmt_close($stmt);
        }
        header("location: stanice_add.php");
    }
}
?>

<!DOCTYPE html>
<html lang="cs">

<head>
    <meta charset="UTF-8">
    <title>Vložení pracovní stanice</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body {
            font: 14px sans-serif;
        }

        .wrapper {
            width: 500px;
            padding: 20px;
        }

        tr.dark {
            background-color: #ddd;
            color: black;
        }

        tr.light {
            background-color: #fff;
            color: black;
        }

    </style>

<script type="text/javascript">
		function lokalita(lok) {
			lok_alita=lok;

			var qry='';

			if (lok_alita != '') {
				qry += 'lok_alita=' + lok_alita;
			}

			var xmlhttp;

			xmlhttp=new XMLHttpRequest();

			xmlhttp.onreadystatechange = function() {
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					document.getElementById("data").innerHTML = xmlhttp.responseText;
				}
			}

			xmlhttp.open("GET",'vypis_filtr.php?'+qry, true);
			xmlhttp.send();
		}

	</script>

</head>

<body onLoad="lokalita('')">
    <?php
$app_up = PageHeader();
?>
    <div class="wrapper">
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group <?php echo (!empty($stanice_err)) ? 'has-error' : ''; ?>">
                <label>Označení stanice:</label>
                <input type="text" name="stanice" class="form-control" value="<?php echo $stanice; ?>" autofocus>
                <span class="help-block"><?php echo $stanice_err; ?></span>
            </div>

            <div class="form-group <?php echo (!empty($lokalita_err)) ? 'has-error' : ''; ?>">
                <label for="lokalita">Lokalita:</label>
                <select class="form-control" id="lokalita" name="lokalita">
                    <option value="">---</option>
                    <?php
$sql = "SELECT id,nazev FROM lokality ORDER BY nazev;";

if ($stmt = mysqli_prepare($link, $sql)) {
    if (mysqli_stmt_execute($stmt)) {
        mysqli_stmt_bind_result($stmt, $loc_id, $loc_name);

        while (mysqli_stmt_fetch($stmt)) {
            echo "<option value=\"$loc_id\"";
            if ($loc_id == $lokalita) {
                echo " SELECTED";
            }
            echo ">$loc_name</option>\n";
        }
    }
}
mysqli_stmt_close($stmt);
?>
                </select>
                <span class="help-block"><?php echo $lokalita_err; ?></span>
            </div>

            <div class="form-group <?php echo (!empty($address_err)) ? 'has-error' : ''; ?>">
                <label>IP adresa:</label>
                <input type="text" name="address" class="form-control" value="<?php echo $address; ?>">
                <span class="help-block"><?php echo $address_err; ?></span>
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Vložit">
            </div>
        </form>
    </div>

    <hr>
    <?php
echo "<table width=\"100%\">";
echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td><select id=\"lokalita\" name=\"lokalita\" onChange=\"lokalita(this.value)\" style=\"width:90px;\"><option value=\"\">---</option>";

$sql = "SELECT id,nazev FROM lokality ORDER BY nazev";

if ($stmt = mysqli_prepare($link, $sql)) {
    if (mysqli_stmt_execute($stmt)) {
        mysqli_stmt_bind_result($stmt, $lok_id, $lok_name);

        while (mysqli_stmt_fetch($stmt)) {
            echo "<option value=\"$lok_id\"";
            if ($lok_id == $lokalita) {
                echo " SELECTED";
            }
            echo ">$lok_name</option>\n";
        }
    }
}
mysqli_stmt_close($stmt);

echo "</select></td><td>&nbsp;</td><td></td></tr></table>";
echo "<div id=\"data\">";
echo "</div>";

mysqli_close($link);
?>