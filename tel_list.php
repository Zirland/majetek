<?php
date_default_timezone_set('Europe/Prague');
session_start();

if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: login.php");
    exit;
}

require_once 'config.php';
?>

<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <title>Přehled telefonů TCTV 112</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }

        tr.dark {
            background-color: #ddd;
            color: black;
        }

        tr.light {
            background-color: #fff;
            color: black;
        }

    </style>
</head>
<body>
<?php
PageHeader();

$i = 0;
echo "<table width = \"100%\">";
echo "<tr><th>Lokalita</th><th>Stanice</th><th>Set ID</th><th>Agent ID</th><th>Typ telefonu</th><th>IP adresa</th><th>MAC adresa</th><th>Sériové číslo</th><th>Poznámka</th></tr>";
$query12 = "SELECT lokality.nazev, stanice.id, set_id, agent_id, tel_typ, telefony.ip_addr, mac, serial_number, poznamka
FROM `telefony`
LEFT JOIN stanice ON telefony.serial_number = stanice.telefon
LEFT JOIN lokality ON telefony.placement = lokality.id
ORDER BY lokality.nazev, set_id, serial_number;";
if ($result12 = mysqli_query($link, $query12)) {
    while ($row12 = mysqli_fetch_row($result12)) {
        $lokalita      = $row12[0];
        $stanice       = $row12[1];
        $set_id        = $row12[2];
        $agent_id      = $row12[3];
        $tel_typ       = $row12[4];
        $ip_addr       = $row12[5];
        $mac           = $row12[6];
        $serial_number = $row12[7];
        $poznamka      = $row12[8];

        echo "<tr class=\"";
        if ($i % 2 == 0) {
            echo "dark";
        } else {
            echo "light";
        }
        echo "\"><td>$lokalita</td><td>$stanice</td><td>$set_id</td><td>$agent_id</td><td>$tel_typ</td><td>$ip_addr</td><td>$mac</td><td>$serial_number</td><td>$poznamka</td></tr>";
        $i = $i + 1;
    }
}

echo "</table>";

mysqli_close($link);
